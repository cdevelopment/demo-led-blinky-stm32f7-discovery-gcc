/*==============================================================================
**
**                      Proprietary - Copyright (C) 2017
**------------------------------------------------------------------------------
** Supported MCUs      : STM32F
** Supported Compilers : GCC
**------------------------------------------------------------------------------
** File name         : main.c
**
** project name      : Demo LED Blinky on STM32F7 using GCC
**
**
** Summary: www.cmq.vn - Embedded Sytem Articles, IOT Technology
**
**= History ====================================================================
** 1.0  11/11/2017  CMQ Team
** - Creation
==============================================================================*/

/******************************************************************************/
/* INCLUSIONS                                                                 */
/******************************************************************************/
#include "mcu.h"
#include "stm32746g_discovery.h"
/*****************************************************************************/
/* DEFINITION OF CONSTANTS                                                   */
/*****************************************************************************/

/******************************************************************************/
/* DECLARATION OF VARIABLES (Only external global variables)                  */
/******************************************************************************/

/******************************************************************************/
/* DECLARATION OF VARIABLES OF MODULE                                         */
/* Declaration of local variables shall have the specifier STATIC             */
/******************************************************************************/


/******************************************************************************/
/* DECLARATION OF LOCAL FUNCTIONS (Internal Functions)                        */
/* Declaration of local functions shall have the specifier STATIC             */
/******************************************************************************/
static void MPU_Config         (void);
static void CPU_CACHE_Enable   (void);
static void SystemClock_Config (void);
/******************************************************************************/
/* DEFINITION OF GLOBAL FUNCTIONS (APIs, Callbacks and MainFunction(s))       */
/******************************************************************************/
int main(void)
{

    MPU_Config();
    
    CPU_CACHE_Enable();
    
    SystemClock_Config();

    /* System Core Clock Update */
    SystemCoreClockUpdate();

    /* init HAL */
    /* Configure Flash prefetch and Instruction cache through ART accelerator */ 
    HAL_Init();

    /* init LED BSP */
    BSP_LED_Init(LED_GREEN);

    
    int counter = 0;
    while(1)
    {
        BSP_LED_On(LED_GREEN);
    	HAL_Delay(500);
        BSP_LED_Off(LED_GREEN);
        HAL_Delay(500);
    }
}

/******************************************************************************/
/* DEFINITION OF LOCAL FUNCTIONS (Internal Functions)                         */
/* Declaration of local functions shall have the specifier STATIC             */
/******************************************************************************/

static void MPU_Config(void)
{
    MPU_Region_InitTypeDef MPU_InitStruct;
    
    /* Disable the MPU */
    HAL_MPU_Disable();
    
    /* Configure the MPU attributes as WT for SRAM */
    MPU_InitStruct.Enable             = MPU_REGION_ENABLE;
    MPU_InitStruct.BaseAddress        = 0x20000000;
    MPU_InitStruct.Size               = MPU_REGION_SIZE_512KB;
    MPU_InitStruct.AccessPermission   = MPU_REGION_FULL_ACCESS;
    MPU_InitStruct.IsBufferable       = MPU_ACCESS_NOT_BUFFERABLE;
    MPU_InitStruct.IsCacheable        = MPU_ACCESS_CACHEABLE;
    MPU_InitStruct.IsShareable        = MPU_ACCESS_NOT_SHAREABLE;
    MPU_InitStruct.Number             = MPU_REGION_NUMBER0;
    MPU_InitStruct.TypeExtField       = MPU_TEX_LEVEL0;
    MPU_InitStruct.SubRegionDisable   = 0x00;
    MPU_InitStruct.DisableExec        = MPU_INSTRUCTION_ACCESS_ENABLE;
    
    HAL_MPU_ConfigRegion(&MPU_InitStruct);
    
    /* Enable the MPU */
    HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

/**
* @brief  CPU L1-Cache enable.
* @param  None
* @retval None
*/
static void CPU_CACHE_Enable(void)
{
    /* Enable I-Cache */
    SCB_EnableICache();
    
    /* Enable D-Cache */
    SCB_EnableDCache();
}


static void SystemClock_Config (void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;

    /* Enable HSE Oscillator and activate PLL with HSE as source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = 25;
    RCC_OscInitStruct.PLL.PLLN = 432;  
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
    RCC_OscInitStruct.PLL.PLLQ = 9;
    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    /* Activate the OverDrive to reach the 216 MHz Frequency */
    HAL_PWREx_EnableOverDrive();

    /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;  
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;  
    HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7);
}

/************************* End of File ****************************************/